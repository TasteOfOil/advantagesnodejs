const express = require('express');
const cors = require('cors');
const app = express();

const port = process.env.PORT || 3000;

app.listen(port, ()=>{
    console.log(`listening on port:${port}`);
});

app.use(cors({
    origin:'*'
}));

app.get('/', (req,res)=>{
    console.log(req.url);
    res.send('Hello World!');
});
